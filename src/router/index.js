import Vue from 'vue'
import Router from 'vue-router'
import mainpage from '@/components/mainpage'
import maingoods from '@/components/maingoods'

Vue.use(Router)


export default new Router({
  routes: [
    {
      path: '/main',
      name: 'mainpage',
      component: mainpage,
      children:[
        {
          path: 'maingoods', //路径： /main/maingoods
          name: 'maingoods',
          component: maingoods
        },
        // {
        //   path: 'maingoodsedit', //路径： /main/maingoods
        //   name: 'maingoodsedit2',//名称：maingoodsedit
        //   component: ()=>import('@/components/maingoodsedit')
        // }
      ]
    },
    // {
    //   path:'/',
    //   name:'goodslist',
    //   //component:()=>import('@/components/goodslist')
    //   components:{
    //     default: ()=>import('@/components/goodslist'),
    //     childTop:()=>import('@/components/Top'),
    //     childBottom:()=>import('@/components/Bootom')
    //   }
    // },
    // {
    //   path:'/test',
    //   name:'test',
    //   component:()=>import('@/components/test')
    // },
    // {
    //   path:'/login',
    //   name:'login',
    //   component:()=>import('@/components/login')
    // }
  ]
})
