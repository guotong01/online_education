// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from "axios";
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';


Vue.config.productionTip = false
//设置axios的相关属性
axios.defaults.baseURL='http://localhost:8089' //http://localhost:8089/goods/findAll
axios.defaults.withCredentials=true //前端允许跨域

//axios拦截--request/response
axios.interceptors.request.use(function (config){
  // Do something before request is sent
  //判断localStorage里是否有令牌
  var token=localStorage.getItem("token");
  console.log(token)
  if (token!=null){
    //设置在请求头部中
    config.headers.token=token;
  }
  return config;
}, function (error){
    //Do something with request error
  return Promise.reject(error);
  });

axios.interceptors.response.use(function (response){
  response=response.data;
  return response;

},function (error){
  return Promise.reject(error);
  });


//并且设置axios全局使用
Vue.prototype.$axios= axios; //this代表vue时，this.$axios.get("/user/info")

Vue.use(ElementUI);//全局使用element


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
